# pihole

Custom domain blocking for my pihole

## Installation from scratch

* Install latest Raspbian image
* Set up local, a reserved DHCP address, etc.
* Check for any apt-get updates/upgrades
* `sudo reboot`
* Install Pi-hole with `curl -sSL https://install.pi-hole.net | bash`

* Install and configure Cloudflare DNS over HTTPS https://docs.pi-hole.net/guides/dns-over-https/

## PADD / Other
* https://github.com/jpmck/PADD
* https://github.com/bpennypacker/phad

## Useful Items
* [What files does Pi-hole use?](https://discourse.pi-hole.net/t/what-files-does-pi-hole-use/1684)
* 